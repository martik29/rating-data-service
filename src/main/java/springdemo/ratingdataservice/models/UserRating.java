package springdemo.ratingdataservice.models;

import java.util.List;

public class UserRating {
    public List<Rating> getRatings;

    //user rating
    public UserRating() {
    }

    public UserRating(List<Rating> getRatings) {
        this.getRatings = getRatings;
    }

    public List<Rating> getGetRatings() {
        return getRatings;
    }

    public void setGetRatings(List<Rating> getRatings) {
        this.getRatings = getRatings;
    }
}
