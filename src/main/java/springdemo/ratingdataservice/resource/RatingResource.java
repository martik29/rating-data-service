package springdemo.ratingdataservice.resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springdemo.ratingdataservice.models.Rating;
import springdemo.ratingdataservice.models.UserRating;

import java.util.Arrays;
import java.util.List;
// REST Controller
@RestController
@RequestMapping("/rating")
public class RatingResource {
    @RequestMapping("/{movieId}")
    public Rating getRating(@PathVariable("movieId") String movieId){
        return new Rating(movieId, 4);
    }

    @RequestMapping("/users/{userId}")
    public UserRating getRatings(@PathVariable("userId") String userId){
        List<Rating> ratings = Arrays.asList(
                new Rating("1234", 7),
                new Rating("5678", 8)
        );
       UserRating userRating = new UserRating();
       userRating.setGetRatings(ratings);
       return userRating;
    }
}
